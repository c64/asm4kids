
  ;;
  ;; Assembly Language for Kids - Page 174
  ;;

  ;; The next programs we will examine, show us two things. First,
  ;; after running the BASIC version of the program, you will see the in
  ;; credible speed of a machine language program that does exactly the
  ;; same thing. Secondly, you will see how we can use several address
  ;; bases within a loop to speed things up. Since the A,X and Y
  ;; registers can only hold 255, by having offsets from the base ad
  ;; dresses, we can put these offsets in our program to access more than
  ;; the base address + 255, indexed by the X register. (Be sure to run
  ;; the BASIC program first, so that you can see what is happening on
  ;; the screen.)

  ;; ACME Assembler
  !to "174-1.o", cbm              ; name of output, type of assembly
  !cpu 6510                     ; type of cpu

  * = $c000

  jsr $e544
  ldx #$00

loop:   txa
  sta $d800,x                     ; Store ACC value in color memory
  sta $d8fa,x
  sta $d9f4,x
  sta $daee,x

  sta $0400,x                     ; Screen address space
  sta $04fa,x
  sta $05f4,x
  sta $06ee,x

  inx
  cpx #$fa                      ; Compare X register value with 250
  bne loop                      ; Branch back to label if not equal

  rts
  
