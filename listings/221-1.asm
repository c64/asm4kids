
  ;;
  ;; Assembly Language for Kids - Page 221
  ;;

  ;; By lining up the various blocks, changing their color to what you
  ;; want, you can create low resolution figures. To get started, let's
  ;; draw a couple of parallel lines in different colors:

  ;; ACME Assembler
  !to "221-1.o", cbm            ; name of output, type of assembly
  !cpu 6510                     ; type of cpu

  * = $c000

	bar1  = $d878
	bar2  = $d8c8
	lin1  = $0478
	lin2  = $04c8
	clear = $e544

	jsr clear
	ldx #$00
	ldy #$02

start: lda #$e0									; Char code of inverted space (224)
	sta lin1,x										; First line of spaces
	sta lin2,x										; Second line of spaces
	tya
	sta bar1,x										; First color of line
	clc
	adc #$05											; Change color with ADC
	sta bar2,x										; Second color of line
	inx
	cpx #$27
	bne start

	rts
