
  ;;
  ;; Assembly Language for Kids - Page 226
  ;;

  ;; In using the PLOT subroutine, you have to be sure to LDA your
  ;; character to be printed on the screen AFTER you PLOT. This is
  ;; because the accumulator can be scrambled by the PLOT subrou
  ;; tine. Also, be sure to CLC before you JSR to PLOT ($FFF0),
  ;; otherwise you may end up reading and not setting the plot location.
  ;; (To read the plot location, you set the carry flag with SEC.)

  ;; ACME Assembler
  !to "226-1.o", cbm            ; name of output, type of assembly
  !cpu 6510                     ; type of cpu

  * = $c000

  chrout = $FFD2
  clear  = $E544
  plot   = $FFF0

  jsr clear
  ldx #$00
  ldy #$00
  lda #$12                      ; Char code inverse
  jsr chrout

start: clc
  jsr plot
  lda #$20                      ; Char code for space
  jsr chrout
  inx                           ; Next row
  iny                           ; Next column
  cpx #$19                      ; Bottom row yet?
  bne start

  rts
