
  ;;
  ;; Assembly Language for Kids - Page 190
  ;;

  ;; Now, this next part is weird; so put down your root beer and
  ;; listen up. When we want to ADC, it's important to clear the carry
  ;; flag with CLC. However, when we subtract from the accumulator
  ;; with SBC we want the carry flag set; so we use SEC to set the carry
  ;; flag. The reason for this is that in subtraction, the set carry flag is
  ;; treated as though no borrow is taken; just the reverse of ADC.
  ;; What you're really doing is called "two's compliment" addition
  ;; since that's the way your 6510 can best handle subtraction. It sort of
  ;; uses the carry flag to "add backwards."

  ;; ACME Assembler
  !to "190-1.o", cbm            ; name of output, type of assembly
  !cpu 6510                     ; type of cpu

  * = $c000

  jsr $e544
  lda #$5d                      ; Load ACC with the value 93

start: sec                      ; Set the carry flag
  sbc #$03                      ; Subtract 3 from ACC
  jsr $e716                     ; Output to the screen
  cmp #$42                      ; Compare ACC value to 66
  bne start                     ; Branch back if not 0

  rts
