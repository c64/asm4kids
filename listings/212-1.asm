
  ;;
  ;; Assembly Language for Kids - Page 212
  ;;

  ;; At the beginning of this chapter we discussed creating prompts. As you
  ;; may have noticed, it took a lot of code to put a simple prompt like
  ;; COLOR? on the screen. With most assemblers, there is an easy way to
  ;; do it using a pseudo-opcode called ASC, .BYTE or some similar
  ;; pseudo-opcode.

  ;; NOTE: !pet or !text are the psydo opcodes for text strings in ACME

  ;; ACME Assembler
  !to "212-1.o", cbm            ; name of output, type of assembly
  !cpu 6510                     ; type of cpu

  * = $c000

  clear  = $e544
  chrout = $ffd2

  jsr clear                     ; Clear screen
  ldx #$00

read:   lda msg,x               ; Load character from string
  jsr chrout                    ; Print to screen
  cpx #$08                      ; Compare to length of message (0-8)
  beq end                       ; If length is reached jump to end
  inx                           ; increment X register
  jmp read                      ; Branch back to beginning of the loop

end: rts
msg: !pet "commodore"           ; string
