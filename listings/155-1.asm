
  ;;
  ;; Assembly Language for Kids - Page 155
  ;;

  ;; To see how we can put this to use, let's pick a location that we can
  ;; see incremented. We know the screen addresses $0400 to $07E7 make up
  ;; the color memory map. By using indexed addressing, we can place
  ;; values in those locations simply by incrementing X or Y and storing
  ;; our values in the starting addresses offset by the X or Y registers.
  
  ;; ACME Assembler
  !to "155-1.o", cbm ; name of output, type of assembly
  !cpu 6510 ; type of cpu

  * = $c000

  jsr $e544
  lda #$04
  sta $d021                     ; background color address
  lda #$01
  ldx #$00
  sta $d800,x                   ; base color address + X value
  sta $0400,x                   ; base screen address + X value
  inx
  sta $d800,x
  sta $0400,x
  inx
  sta $d800,x
  sta $0400,x
  rts
  
  
