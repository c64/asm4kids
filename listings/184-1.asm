
  ;;
  ;; Assembly Language for Kids - Page 184
  ;;

  ;; To see how INC works, we'll write a program that will run
  ;; through the alphabet for us.

  ;; ACME Assembler
  !to "184-1.o", cbm            ; name of output, type of assembly
  !cpu 6510                     ; type of cpu

  * = $c000

  lda #$40                      ; Load ACC with the value 64
  sta $c100                     ; Store ACC value in $c100

start: inc $c100                ; Increment the value in $c100
  lda $c100                     ; Load ACC with the value in $c100
  jsr $e716                     ; Output value to screen memory
  cmp #$5a                      ; Compare ACC with 90
  bne start

  rts
