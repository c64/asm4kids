
  ;;
  ;; Assembly Language for Kids - Page 231
  ;;

  ;; Animation in assembly language requires some programming
  ;; concentration, but it's so spectacular when you're finished, it's
  ;; worth it. All animation is based on the illusion of drawing a figure
  ;; in one place, erasing it, and drawing it in another place. It works
  ;; just like cartoons in the movies. A figure is drawn, shown for an in
  ;; stant on the screen, and then it is removed (erased) and another
  ;; figure is put in its place.

  ;; ACME Assembler
  !to "234-1.o", cbm            ; name of output, type of assembly
  !cpu 6510                     ; type of cpu

  * = $c000

  chrout = $FFD2
  clear  = $E544
  plot   = $FFF0
  ex     = $C200
  why    = $C202
  ball   = $C204
  space  = $C206

  jsr clear
  ldx #$0A                      ; Set row to 10
  stx ex
  ldy #$00                      ; Set column to 0
  sty why
  lda #$71                      ; Char code for ball (113)
  sta ball
  lda #$20                      ; Char code for space (32)
  sta space

start: ldx ex
  ldy why
  clc
  jsr plot                      ; Plot the ball
  lda ball                      ; Load the ball
  jsr chrout                    ; Print the ball

  ldy #$00                      ; Begin pause loop

pause1:   ldx #$00

pause2:   inx
  cpx #$FE
  bne pause2

  iny
  cpy #$0A
  bne pause1                    ; End pause loop

  
  ldx ex                        ; Load X register with last row plot
  ldy why                         ; Load Y register with last column plot
  clc
  jsr plot                      ; Plot the space
  lda space                       ; Load the space
  jsr chrout                    ; Erase the ball with a space
  inc why                         ; Increment the column value (y)
  ldy why                         ; Load Y register with next column
  cpy #$26                      ; Is it near the last column
  bne start                       ; If not print and erase another ball

  clc
  jsr plot
  lda ball
  jsr chrout                    ; Put a ball on the screen so there is something left

  rts
