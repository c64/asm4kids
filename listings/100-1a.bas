1  REM
2  REM - ASSEMBLY LANGUAGE FOR KIDS - PAGE 100
3  REM -= BIN/DEC CONVERTER =-
4  REM
10 PRINT CHR$(147)
20 INPUT "BINARY VALUE "; B$
25 IF B$="Q" THEN END
30 IF LEN(B$) <> 8 THEN 20
40 FOR X=0 TO 7
50 V$ = MID$(B$, X+1, 1)
60 V = VAL(V$) :REM ORIGINAL B0RKED HERE
70 P=7-X
80 IF V=1 THEN BV=2^P :REM UPWARD ARROW KEY
90 TD=TD+BV
100 BV=0
110 NEXT X
120 PRINT "DECIMAL VALUE = ";TD
130 INPUT "ANOTHER(Y/N) ";AN$
140 IF AN$="Y" THEN TD=0 : GOTO 20
