
  ;;
  ;; Assembly Language for Kids - Page 172
  ;;

  ;; The loop allowed us to use the base address for the character and
  ;; color screen, and indexing by X, store all 255 characters to the
  ;; screen. Using the Y value, we did the same thing with the corresponding
  ;; color addresses on the screen.

  ;; ACME Assembler
  !to "172-1.o", cbm            ; name of output, type of assembly
  !cpu 6510                     ; type of cpu

  * = $c000

  jsr $e544
  ldx #$00                      ; Load X register with value 0
  ldy #$01                      ; Load Y register with value 1

start:  tya                     ; Transfer Y register value to accumulator
  sta $d800,x                   ; Store ACC value in 'indexed absolute addressing'
  txa                           ; Transfer X register value to accumulator
  sta $0400,x                   ; Store ACC value in 'indexed absolute addressing'
  inx                           ; Increment X register value
  cpx #$ff                      ; Compare X register value to 255
  bne start                     ; Branch back to label if not equal

  rts
