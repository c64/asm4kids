
  ;;
  ;; Assembly Language for Kids - Page 131
  ;;

  ;; The following example loads different values into the
  ;; accumulator and prints my first name, "BILL".
  
  ;; ACME Assembler
  !to "131-1-bill.o", cbm       ; name of output, type of assembly
  !cpu 6510                     ; type of cpu

  * = $C000

  JSR $E544
  LDA #$42                      ; B
  JSR $E716
  LDA #$49                      ; I
  JSR $E716
  LDA #$4C                      ; L
  JSR $E716
  JSR $E716
  RTS
