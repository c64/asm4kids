
  ;;
  ;; Assembly Language for Kids - Page 141
  ;;

  ;; A final place to STA values is right on your screen! Your screen
  ;; memory 40 x 25 matrix is located from 1024-2023 ($400-$7E7).
  ;; Overlaying the screen memory on the same matrix is the Color
  ;; Memory from 55296-56295 ($D800-$DBE7). When you store
  ;; a character on the screen, you also have to store a color or you
  ;; won't be able to see the character.
  
  ;; ACME Assembler
  !to "141-1.o", cbm ; name of output, type of assembly
  !cpu 6510 ; type of cpu

  * = $c000

  jsr $e544
  lda #$00                      ; load 0 into the ACC (black)
  sta $d800                     ; 1st color address
  lda #$58                      ; spade (char set 1)
  sta $0400                     ; 1st screen position
  rts
  
