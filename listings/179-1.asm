
  ;;
  ;; Assembly Language for Kids - Page 179
  ;;

  ;; In the below program, locations $C200 and $8000 have variable
  ;; values stored. The X register is incremented until it is equal to the
  ;; contents of $C200. When X equals that amount, X is compared
  ;; with the contents of $8000. If they're equal, the program takes
  ;; PATH2; otherwise it takes PATH1. At the end of PATH1, it
  ;; JuMPs over PATH2 to the END of the program. (The above pro
  ;; gram is hypothetical to illustrate a point. If you want to run it, 
  ;; provide values for $C200 and $8000.)

  ;; ACME Assembler
  !to "179-1.o", cbm            ; name of output, type of assembly
  !cpu 6510                     ; type of cpu

  * = $c000

  ldx #$00
  lda #$0a                      ; play with these values
  sta $c200
  lda #$0f                      ; play with these values
  sta $8000
  
loop: inx
  cpx $c200                     ; Loop until X equals contents of $c200
  bne loop

  cpx $8000                     ; Compare X to the contents of $8000
  beq path2                     ; If equal branch to 'path2'

path1: inx
  txa
  jsr $e716
  jmp end                       ; Jump over 'path2' to 'end'

path2: dex
  txa
  jsr $e716

end: rts
  
