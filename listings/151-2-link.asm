
  ;;
  ;; Assembly Language for Kids - Page 151
  ;;

  ;; The next two assembly programs will allow you to LOAD a BASIC file
  ;; into memory and not destroy the contents of memory. Actually, it
  ;; loads the program onto the end of the program in memory. This is
  ;; done simply by resetting the pointers showing the beginning of the
  ;; BASIC program to the end of the program in memory. Then, a second
  ;; machine language program resets the pointers to the actual beginning
  ;; of BASIC thereby linking the two programs together.
  
  ;; ACME Assembler
  !to "151-1-link.o", cbm ; name of output, type of assembly
  !cpu 6510 ; type of cpu

  * = $C000

  lda #$01
  sta $2B
  lda #$08
  sta $2C
  rts
