
  ;;
  ;; Assembly Language for Kids - Page 170
  ;;

  ;; Now the first thing to notice in the programs is the different ways
  ;; in which the branch was used. Generally, assemblers will have pro
  ;; visions for a LABEL field. In that field you can label your lines,
  ;; with the label serving as an address.

  ;; ACME Assembler
  !to "170-1.o", cbm ; name of output, type of assembly
  !cpu 6510 ; type of cpu

  * = $c000

  jsr $e544
  ldx #$41                      ; Initialize X register with value 65
loop: txa                       ; Transfer X register value to accumulator
  jsr $e716                     ; Output to screen with CHR$
  inx                           ; Increment X register value
  cpx #$5b                      ; Compare X register value to 91
  bne loop                      ; If not equal branch back to 'loop'
  rts
  
