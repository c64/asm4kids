
  ;;
  ;; Assembly Language for Kids - Page 185
  ;;

  ;; The results of the previous program may surprise you. At first, you may
  ;; have thought you'd get the alphabet printed to the screen and then
  ;; stored in sequential addresses from $C100-$C11A (49408-49434).
  ;; That would make a nice table, but that's not what we got. Instead,
  ;; as you can see from the mess on your screen, after getting the *A* as
  ;; expected, the rest is garbage, the reason for that lies in the fact that
  ;; we were changing the addresses and INCrementing each new ad
  ;; dress by 1. Thus, while we stored the value 64 in $C100 and then in
  ;; cremented it by 1 to get 65 (the 'A' you saw on your screen), we then
  ;; INCremented $C101, which was empty (or full of junk) by 1. That
  ;; junk was sent to the accumulator and output to the screen. We'll
  ;; need some new instructions to get what we need. (I'll bet you're
  ;; smart enough to figure out how to get the alphabet stored in a table
  ;; without any new instructions, though. Have the Y register help you
  ;; out.)

  ;; ACME Assembler
  !to "185-2.o", cbm            ; name of output, type of assembly
  !cpu 6510                     ; type of cpu

  * = $c000

  ldx #$00                      ; Load X register with the value 0
  ldy #$40                      ; Load ACC with the value 64
  tya
  sta $c100                     ; Store ACC value in $c100

start: iny
  tya
  sta $c100,x
  inc $c100,x                   ; Increment value in $c100
  lda $c100,x
  jsr $e716
  inx
  cpx #$5a
  bne start

  rts
