
  ;;
  ;; Assembly Language for Kids - Page 201
  ;;

  ;; Once we have the prompt, we can use our SCNKEY and GETIN
  ;; subroutines to read the keyboard and put the results in the ac
  ;; cumulator. Now, since we want to change the background color,
  ;; we will JSR to $D021. However, the background colors 0-15 re
  ;; quire CTRL keys, and we want to use regular keys. To keep it
  ;; relatively simple, we'll just change to background colors to (R)ed or
  ;; (G)reen. If *R' is pressed, the background will turn red and *G' will
  ;; turn it green. Also, to get out of the program, RETURN will cause
  ;; an exit.

  ;; ACME Assembler
  !to "201-1.o", cbm            ; name of output, type of assembly
  !cpu 6510                     ; type of cpu

  * = $c000

  clear   = $e544
  scnkey  = $ff9f
  getin   = $ffe4
  chrout  = $ffd2
  bkgnd   = $d021

  jsr clear
  lda #$43                      ; C
  jsr chrout
  lda #$4f                      ; O
  jsr chrout
  lda #$4c                      ; L
  jsr chrout
  lda #$4f                      ; O
  jsr chrout
  lda #$52                      ; R
  jsr chrout
  lda #$3f                      ; ?
  jsr chrout
  lda #$00                      ; Null the ACC

scan: jsr scnkey
  jsr getin
  beq scan
  cmp #$0d                      ; RETURN pressed
  beq end                       ; If so goto 'end'

  cmp #$52                      ; R pressed
  beq red                       ; If so goto 'red'

  cmp #$47                      ; G pressed
  beq green                     ; If so goto 'green'

  jmp scan                      ; None of the above go back to work

red: lda #$02                   ; Color code for red
  sta bkgnd
  jmp scan                      ; Back to work

green: lda #$05                 ; Color code for green
  sta bkgnd
  jmp scan                      ; Go get another key

end: rts
