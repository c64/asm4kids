
  ;;
  ;; Assembly Language for Kids - Page 159
  ;;

  ;; Zero page addressing has special machine language op
  ;; codes that are not apparent in most assemblers. The STA
  ;; instruction in the absolute mode has one opcode for non
  ;; zero page addressing and another for zero page address
  ;; ing. Thus, STA instructions for $100 and higher are in
  ;; terpreted as machine opcode $8D, but for locations of
  ;; $0FF and lower, the machine opcode $85 is used. Zero-
  ;; page addressing saves one byte compared with non-zero
  ;; page addressing since the operand address is one byte
  ;; ($FF or less.)

  ;; The below program is a pretty weird way to get a crummy 'A'
  ;; printed to the screen. However, it shows what must be done to set
  ;; up indexed indirect addressing. First, the target address must be
  ;; given a value to use. Second, the low and high byte of the target ad
  ;; dress must be stored in zero-page. Finally, the X Register must be
  ;; given a value. After that is done, indexed indirect addressing is
  ;; possible. When you begin using tables of numbers, you can use the
  ;; X Register as an indirect pointer to the table. In the meantime, I
  ;; wouldn't spend a lot of time trying to use this mode.

  ;; ACME Assembler
  !to "159-1.o", cbm            ; name of output, type of assembly
  !cpu 6510                     ; type of cpu

  * = $c000

  jsr $e544
  ldy #$41                      ; Load Y register  with ASCII A
  sty $c100                     ; Store Y value in address $c100
  lda #$00                      ; Low byte of target address
  sta $fb                       ; Store in low byte pointer address
  lda #$c1                      ; High byte of target address
  sta $fc                       ; Store in high byte pointer address
  ldx #$00                      ;
  lda ($fb,x)                   ; Indexed indirect LDA
  jsr $e716                     ; Output char to screen

  rts
  
