
  ;;
  ;; Assembly Language for Kids - Page 223
  ;;

  ;; Since horizontal lines are so simple, vertical lines ought to be a
  ;; snap. Unfortunately, it doesn't work quite that way. With horizon
  ;; tal lines, we could run one all the way across the screen using the X
  ;; register as an offset from the beginning address. However, with ver
  ;; tical lines, our address jumps from the top of the screen to the bot
  ;; tom are greater than 255. In fact, there is a difference of 40 between
  ;; each address we will need.

  ;; ACME Assembler
  !to "223-1.o", cbm            ; name of output, type of assembly
  !cpu 6510                     ; type of cpu

  * = $c000

	bar1  = $D814
	bar2  = $D904
	bar3  = $D9F4
	bar4  = $DAE4
	bar5  = $DBD4

	lin1  = $0414
	lin2  = $0504
	lin3  = $052C
	lin4  = $061C
	lin5  = $070C

	clear = $E544

	jsr clear
	ldx #$00

start: lda #$E0
	sta lin1,x
	sta lin2,x
	sta lin3,x
	sta lin4,x
	sta lin5,x
	lda #$02
	sta bar1,x
	sta bar2,x
	sta bar3,x
	sta bar4,x
	sta bar5,x
	ldy #$00

inxr:	inx
	iny
	cpy #$28											; 40
	bne inxr
	cpx #$F0
	bne start

	rts
