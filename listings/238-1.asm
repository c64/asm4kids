
  ;;
  ;; Assembly Language for Kids - Page 238
  ;;

  ;; Now that we have seen how to move objects, let's see how to con
  ;; trol them with an external device. We'll use the joystick for our ex
  ;; amples, but you could do the same thing with the keyboard. Just
  ;; substitute the SCNKEY and GETIN routines for the joystick ones
  ;; in the following programs.

  ;; The nice thing about using the PLOT subroutine in animation is
  ;; that you can use the X and Y registers to place things on the screen
  ;; in sequential locations. However, when you start moving all over
  ;; the screen, PLOT can really scramble things, especially your brain.
  ;; Therefore, we will begin using the ASCII code for your cursor con
  ;; trol.

  ;; ACME Assembler
  !to "238-1.o", cbm            ; name of output, type of assembly
  !cpu 6510                     ; type of cpu

  * = $c000

  clear  = $E544
  jstick = $DC01
  chrout = $FFD2
  offset = $C200
  fire   = $C202
  invrs  = $C204
  normal = $C206
  mark   = $C208

  jsr clear
  lda #$FF                        ; OR mask
  sta offset
  lda #$10                      ; Fire button
  sta fire
  lda #$20                      ; Space for the cursor
  sta mark
  lda #$12                      ; Inverse code
  sta invrs
  lda #$92                      ; Normal code
  sta normal

start:  lda jstick
  eor offset
  cmp #$01                      ; Joystick up?
  beq up
  cmp #$02                      ; Joystick down?
  beq down
  cmp #$04                      ; Joystick left?
  beq left
  cmp #$08                      ; Joystick right?
  beq right

  cmp fire                      ; Fire button pressed?
  beq end                         ; If so then end

cursor:   lda mark                ; Load the space
  jsr chrout                    ; Print the space
  lda #$9D                      ; Load the left cursor
  jsr chrout                    ; Back up
  lda normal
  jsr chrout                    ; Set normal
  lda mark
  jsr chrout
  lda #$9D
  jsr chrout
  lda invrs
  jsr chrout
  jmp start                       ; Go do it again

up:   lda #$91
  jmp print                       ; Print up cursor

down:   lda #$11
  jmp print                       ; Print down cursor

left:   lda #$9D
  jmp print                       ; Print left cursor

right: lda #$1D

print: jsr chrout
  jmp cursor

end: rts
