
  ;;
  ;; Assembly Language for Kids - Page 180
  ;;

  ;; All the program does is print the alphabet to your screen by in
  ;; crementing X until it reaches the value 90. We could have done the
  ;; same thing with BNE and used less code, but it did allow us to place
  ;; the INX after the comparison and branch instructions. The main
  ;; point of the program was to illustrate how BEQ and JMP work.

  ;; ACME Assembler
  !to "180-1.o", cbm            ; name of output, type of assembly
  !cpu 6510                     ; type of cpu

  * = $c000

  ldx #$41

start: txa
  jsr $e716
  cmp #$5a
  beq end

  inx
  jmp start

end: rts

