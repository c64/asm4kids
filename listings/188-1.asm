
  ;;
  ;; Assembly Language for Kids - Page 188
  ;;

  ;; For example, the following program will print 'AZ' to your screen by
  ;; adding 25 to the accumulator which already contains 65:

  ;; ACME Assembler
  !to "188-1.o", cbm            ; name of output, type of assembly
  !cpu 6510                     ; type of cpu

  * = $c000

  jsr $e544                     ; Clear the screen
  lda #$3f                      ; Load ACC with the value 63

start: clc                      ; Clear carry flag
  adc #$02                      ; Add 2 to the value of ACC
  jsr $e716                     ; Output to the screen
  cmp #$59                      ; Compare ACC value to 89
  bne start                     ; Branch if not zero

  rts
