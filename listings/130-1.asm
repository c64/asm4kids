
  ;;
  ;; Assembly Language for Kids - Page 130
  ;;

  ;; ACME Assembler
  !to "130-1.o", cbm      ; name of output, type of assembly
  !cpu 6510                     ; type of cpu

  * = $C000

  jsr $e544 ; clear screen
  lda #88   ; load accumulator
  jsr $e716 ; output to screen
  rts
