
  ;;
  ;; Assembly Language for Kids - Page 185
  ;;

  ;; To see how INC works, we'll write a program that will run
  ;; through the alphabet for us.

  ;; ACME Assembler
  !to "185-1.o", cbm            ; name of output, type of assembly
  !cpu 6510                     ; type of cpu

  * = $c000

  ldx #$00                      ; Load X register with the value 0
  lda #$40                      ; Load ACC with the value 64
  sta $c100                     ; Store ACC value in $c100

start: inc $c100,x              ; Increment value in memory $c100+X
  lda $c100,x                   ; Load ACC with the value from $c100+X
  jsr $e716                     ; Output value in ACC to screen
  inx                           ; Increment X register
  cpx #$5a                      ; Compare X register value to 90
  bne start                     ; Branch if not 0

  rts
