
  ;;
  ;; Assembly Language for Kids - Page 010
  ;;

  ;; ACME Assembler
  !to "010-1.o", cbm            ; name of output, type of assembly
  !cpu 6510                     ; type of cpu

  * = $c000

  jsr $e544
  rts
