
  ;;
  ;; Assembly Language for Kids - Page 210
  ;;

  ;; Now that we can easily read the joystick, let's do something with
  ;; it. In the next chapter we'll see how to move graphics and sprites
  ;; with the joystick, but for now we'll just use it to change the
  ;; background colors. This time, though, we will not use ADC for an
  ;; offset to the ASCII code. Instead, we'll see what background colors
  ;; are created with the different joystick positions.

  ;; ACME Assembler
  !to "210-1.o", cbm            ; name of output, type of assembly
  !cpu 6510                     ; type of cpu

  * = $c000

  jstick = $dc01
  offset = $c100
  clear  = $e544
  fire   = $c102

  jsr clear
  lda #$ff                      ; Value for EOR
  sta offset                    ; EOR offset
  lda #$10                      ; EORed value of fire button
  sta fire                      ; Store for easy reference

start: lda jstick               ; Read joystick values
  eor offset                    ; EOR with $FF
  cmp fire                      ; Fire button pressed?
  beq end                       ; If fire is pressed then quit

  sta $d021                     ; Put joystick value in bg color register
  jmp start

end: lda #$06                   ; Load ACC with value 6 (color code for blue)
  sta $d021                     ; Set background to standard blue
  rts
