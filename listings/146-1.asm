
  ;;
  ;; Assembly Language for Kids - Page 146
  ;;

  ;; Likewise, you can transfer the Y register to the X register using the
  ;; TYA-TAX sequence. Let's take a look at a program that will show
  ;; you some work with these registers. See if you can guess what will
  ;; be printed on your screen before you SYS your program.

  ;; ACME Assembler
  !to "146-1.o", cbm ; name of output, type of assembly
  !cpu 6510 ; type of cpu

  * = $C000

  jsr $E544                     ; clear screen
  ldx #$54                      ; put value (84 = T) in X
  txa                           ; transfer X register to ACC
  jsr $E716                     ; write ACC to screen memory
  ldx #$41                      ; load X with 65 (A)
  txa                           ; transfer X register to ACC
  sta $C030                     ; store ACC in memory position $c030
  ldy $C030                     ; load value from memory $C030 into Y
  tya                           ; transfer Y register to ACC
  jsr $E716                     ; write ACC to screen memory
  ldy #$58                      ; load Y register with 88 (X)
  tya                           ; transfer Y register to ACC
  jsr $E716                     ; write ACC to screen memory
  rts
  
  
