1  REM
2  REM - ASSEMBLY LANGUAGE FOR KIDS - PAGE 014
3  REM
10 BC=53281 :REM ADDRESS OF BACKGROUND COLOR
20 LB=BC-INT(BC/256)*256 :REM LOW BYTE
30 HB=INT(BC/256) :REM HIGH BYTE
40 POKE 49152,169 :REM LDA
50 POKE 49153,0 :REM BLACK COLOR CODE
60 POKE 49154,141 :REM STA
70 POKE 49155,LB :REM LOW BYTE ADRS
80 POKE 49156,HB :REM HIGH BYTE ADRS
90 POKE 49157,96 :REM RTS
