
  ;;
  ;; Assembly Language for Kids - Page 071
  ;;

  ;; ACME Assembler
  !to "071-2.o", cbm            ; name of output, type of assembly
  !cpu 6510                     ; type of cpu

  * = $C000

start:  jsr $E544
        lda #0                  ; Color code for black
        sta $D021               ; Store in background register
        lda #4                  ; Color code for purple
        sta $D020               ; Store in border register
        lda #5                  ; ASCII controll code for white letters
        jsr $E716               ; Output to screen

end:    rts
