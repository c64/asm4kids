
  ;;
  ;; Assembly Language for Kids - Page 197
  ;;

  ;; When you run this program, be sure to try out your CTRL keys
  ;; was well as you regular keys. Turn on the RVS (reverse) and take it
  ;; through its paces.

  ;; ACME Assembler
  !to "197-1.o", cbm            ; name of output, type of assembly
  !cpu 6510                     ; type of cpu

  * = $c000

  clear   = $e544
  scnkey  = $ff9f
  getin   = $ffe4
  chrout  = $ffd2

  jsr clear

scan:   jsr scnkey
  jsr getin
  beq scan
  cmp #$0d                      ; Compare with ASCII for RETURN (13)
  beq end                       ; Jump to 'end' if RETURN is pressed

  jsr chrout                    ; Print char to screen
  jmp scan                      ; Jump back to scanning

end: rts
