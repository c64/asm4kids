1  REM
2  REM - ASSEMBLY LANGUAGE FOR KIDS - PAGE 007
3  REM
10 C = 58692 :REM DECIMAL VALUE FOR $E544
20 LB = C-INT(C/256)*256 :REM LO-BYTE
30 HB = INT(C/256) :REM HI-BYTE
40 POKE 49152,32 :REM JSR
50 POKE 49153,LB
60 POKE 49154,HB
70 POKE 49155,96 :REM RTS
