
  ;;
  ;; Assembly Language for Kids - Page 187
  ;;

  ;; For example, the following program will print 'AZ' to your screen by
  ;; adding 25 to the accumulator which already contains 65:

  ;; ACME Assembler
  !to "187-1.o", cbm            ; name of output, type of assembly
  !cpu 6510                     ; type of cpu

  * = $c000

  lda #$41                      ; Load ACC with the value 65
  jsr $e716                     ; Output to screen memory
  adc #$19                      ; ADD 25 to the ACC value
  jsr $e716                     ; Output to screen memory

  rts
