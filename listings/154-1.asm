
  ;;
  ;; Assembly Language for Kids - Page 151
  ;;

  ;; INDEXED ABSOLUTE ADDRESSING
  ;; Basically, the way indexed addressing works with your programs
  ;; is to add the value of the X or Y register to the address in the
  ;; operand. For example, look at the following program:
  
  ;; ACME Assembler
  !to "154-1.o", cbm ; name of output, type of assembly
  !cpu 6510 ; type of cpu

  * = $C000

  LDX #$00
  TXA
  STA $0400,X                   ; store X register value at $0400
  INX                           ; increment X register
  TXA
  STA $0400,X                   ; sotre X register value in $0401
