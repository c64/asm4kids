
  ;;
  ;; Assembly Language for Kids - Page 051
  ;;

  ;; background border and character colors
  
  ;; ACME Assembler
  !to "051-1.o", cbm      ; name of output, type of assembly
  !cpu 6510               ; type of cpu

  * = $C000

   JSR $E544
   LDA #0
   STA $D021
   LDA #4
   STA $D020
   LDA #5
   JSR $E716
   RTS
