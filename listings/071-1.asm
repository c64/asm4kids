
  ;;
  ;; Assembly Language for Kids - Page 071
  ;;

  ;; ACME Assembler
  !to "071-1.o", cbm            ; name of output, type of assembly
  !cpu 6510                     ; type of cpu

  * = $C000

   JSR $E544                    ; Jumps to clear routine
   LDA #144
   JSR $E716                    ; Jumps to screen output
   LDA #65
   JSR $E716
   LDA #66
   JSR $E716
   RTS                          ; Returns to BASIC

