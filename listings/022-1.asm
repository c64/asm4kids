
  ;;
  ;; Assembly Language for Kids - Page 022
  ;;

  ;; ACME Assembler
  !to "022-1.o", cbm            ; name of output, type of assembly
  !cpu 6510                     ; type of cpu

  * = $c000

  jsr $e544
  lda #$08
  sta $d021
  rts
