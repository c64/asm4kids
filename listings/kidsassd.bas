10 poke 53281,1 : poke 53280,1 : print chr$(144)
20 print chr$(147)
30 dim dec%(151), opcode$(151), byte%(151)
40 gosub 2510
50 for i = 0 to 150 : read dec%(i) : read opcode$(i) : read byte%(i)
60 next i
70 print chr$(146); chr$(147)
80 print "adrs"; tab(10); "opcode"; tab(25); "operand"
90 for x = 1 to 40 : print chr$(114);: next
100 print
110 rem *********************************
120 rem * set address and input opcode
130 rem *********************************
140 sa = 0 : print "press {return} to default to 49152"
150 input "starting addr"; sa : if sa = 0 then sa = 49152
160 ba = sa
170 print sa; tab(10);
180 input oc$ : if oc$ = "q" then 740
190 c = 0
200 if oc$ = opcode$(c) then d% = dec%(c) : b% = byte%(c) : goto 230
210 c = c+1 : if c = 152 then print tab(10); chr$(18); "error"; chr$(146) : goto 170
220 goto 200
230 if b% = 1 then poke sa,d% : sa = sa+1 : goto 170
240 rem *********************************
250 rem * enter operand
260 rem *********************************
270 print tab(25);: print chr$(145);: input opr$
280 if left$(0pr$,1) <> "$" then oper = val(opr$)
290 if left$(opr$,1) = "$" then gosub 450
300 if oper > 65535 then gosub 590 : oper = 0 : goto 270
310 if oc$ = "bne" or oc$ = "beq" then gosub 660
320 if oc$ = "bcc" or oc$ = "bcs" then gosub 660
330 if oc$ = "bpl" or oc$ = "bmi" then gosub 660
340 if oc$ = "bvc" or oc$ = "bvs" then gosub 660
350 if bf = 1 then bf = 0 : goto 270
360 if oper > 255 and b% < 3 then gosub 520 : oper = 0 : goto 270
370 if oper > 255 then gosub 600
380 rem *********************************
390 rem * compile code
400 rem *********************************
410 if b% = 2 then poke sa,d% : sa = sa + 1
420 if b% = 2 then poke sa,oper : sa = sa +1 : oper = 0 : goto 170
430 poke sa,d% : sa = sa +1
440 poke sa,lb : sa = sa +1 : poke sa,hb : sa = sa + 1 : oper = 0 : goto 170
450 rem *********************************
460 rem * convert hex to decimal
470 rem *********************************
480 h$ = mid$(oper$,2)
490 for l = 1 to len(h$) : hd = asc(mid$(h$,l,1))
500 oper = oper*16+hd-48 + ((hd> 57)*7)
510 next l : return
520 rem *********************************
530 rem * error trap
540 rem *********************************
550 print chr$(18); "error-must be less than 256"
560 for w = 1 to 400 : next w : print chr$(146);: print chr$(145);
570 for x = 1 to 27 : print chr$(32);: next
580 print chr$(157); chr$(157); chr$(145) : return
590 print chr$(18); "value over 65535 ($ffff)"; chr$(146) : return
600 rem *********************************
610 rem * convert to 2 byte number
620 rem *********************************
630 lb = oper - int(oper/256)*256
640 hb = int(oper/256)
650 return
660 rem *********************************
670 rem * branch offset
680 rem *********************************
690 if sa > oper and sa - oper > 128 then print "branch too far" : bf = 1 : oper = 0 : return
700 if sa > oper and oper - sa > 127 then print "branch too far" : bf = 1 : oper = 0 : return
710 if sa > oper then oper = 254-(sa-oper)
720 if sa < oper then oper = (oper-sa)-2
730 return
740 rem *********************************
750 rem * ending routine - disk
740 rem *********************************
770 nb = sa-ba
780 print chr$(147)
790 for x = 1 to 5 : print : next
800 input "save program(y/n)"; an$
810 if an$ = "y" then 870
820 print : print : print "program is"; nb; "bytes long"
830 print "to execute 'sys'"; ba : print
840 input "(b)egin again or (e)nd"; de$
850 if de$ = "b" then 70
860 print: print "end" : end
870 print chr$(147) : for x = 1 to 5 : print : next
880 lb = ba - int(ba/256)*256 : hb = int(ba/256)
890 input "enter file name"; nf$ : nf$ = "0:" + nf$ + str$(ba) + ",p,w"
900 open 2,8,2,nf$
910 print#2,chr$(lb) + chr$(hb);
920 for x = ba to sa-1 : oc = peek(x)
930 print#2,chr$(oc);
940 next x
950 close 2
960 goto 820
970 rem *********************************
980 rem * opcode data
990 rem *********************************
1000 data 0,brk,1
1010 data 1,(ora-x),2
1020 data 5,ora-z,2
1030 data 6,asl-z,2
1040 data 8,php,1
1050 data 9,ora#,2
1060 data 10,asl-a,1
1070 data 13,ora,3
1080 data 14,asl,3
1090 data 16,bpl,2
1100 data 17,(0ra-y),1
1110 data 21,0ra-zx,2
1120 data 22,asl-zx,2
1130 data 24,clc,1
1140 data 25,ora-y,3
1150 data 29,ora-x,3
1160 data 30,asl-x,3
1170 data 32,jsr,3
1180 data 33,(and-x),2
1190 data 36,bit-z,2
1200 data 37,and-z,2
1210 data 38,rol-z,2
1220 data 40,plp,1
1230 data 41,and#,2
1240 data 42,rol-a,1
1250 data 44,bit,3
1260 data 45,and,3
1270 data 46,rol,3
1280 data 48,bmi,2
1290 data 49,(and-y),2
1300 data 53,and-zx,2
1310 data 54,rol-zx,2
1320 data 56,sec,1
1330 data 57,and-y,3
1340 data 61,and-x,3
1350 data 62,rol-x,3
1360 data 64,rti,1
1370 data 65,(eor-x),2
1380 data 69,eor-z,2
1390 data 70,lsr-z,2
1400 data 72,pha,1
1410 data 73,eor#,2
1420 data 74,lsr-a,1
1430 data 76,jmp,3
1440 data 77,eor,3
1450 data 78,lsr,3
1460 data 80,bvc,2
1470 data 81,(e0r-y),2
1480 data 85,eor-zx,2
1490 data 86,lsr-zx,2
1500 data 88,cli,1
1510 data 89,eor-y,3
1520 data 93,eor-x,3
1530 data 94,lsr-x,3
1540 data 96,rts,1
1550 data 97,(adc-x),2
1560 data 101,adc-z,2
1570 data 102,ror-z,2
1580 data 104,pla,1
1590 data 105,adc#,2
1600 data 106,ror-a,1
1610 data 108,(jmp),3
1620 data 109,adc,3
1630 data 110,ror,3
1640 data 112,bvs,2
1650 data 113,(adc-y),2
1660 data 117,adc-zx,2
1670 data 118,ror-zx,2
1680 data 120,sei,1
1690 data 121,adc-y,3
1700 data 125,adc-x,3
1710 data 126,ror-x,3
1720 data 129,(sta-x),2
1730 data 132,sty-z,2
1740 data 133,sta-z,2
1750 data 134,stx-z,2
1760 data 136,dey,1
1770 data 138,txa,1
1780 data 140,sty,3
1790 data 141,sta,3
1800 data 142,stx,3
1810 data 144,bcc,2
1820 data 145,(sta-y),2
1830 data 148,sty-zx,2
1840 data 149,sta-zx,2
1850 data 150,stx-zx,2
1860 data 152,tya,1
1870 data 153,sta-y,3
1880 data 154,txs,1
1890 data 157,sta-x,3
1900 data 160,ldy#,2
1910 data 161,(lda-x),2
1920 data 162,ldx#,2
1930 data 164,ldy-z,2
1940 data 165,lda-z,2
1950 data 166,ldx-z,2
1960 data 168,tay,1
1970 data 169,lda#,2
1980 data 170,tax,1
1990 data 172,ldy,3
2000 data 173,lda,3
2010 data 174,ldx,3
2020 data 176,bcs,2
2030 data 177,(lda-y),2
2040 data 180,ldy-zx,2
2050 data 181,lda-zx,2
2060 data 182,ldx-zy,2
2070 data 184,clv,1
2080 data 185,lda-y,3
2090 data 186,tsx,1
2100 data 188,ldy-x,3
2110 data 189,lda-x,3
2120 data 190,ldx-y,3
2130 data 192,cpy#,2
2140 data 193,(cmp-x),2
2150 data 196,cpy-z,2
2160 data 197,cmp-z,2
2170 data 198,dec-z,2
2180 data 200,iny,1
2190 data 201,cmp#,2
2200 data 202,dex,1
2210 data 204,cpy,3
2220 data 205,cmp,3
2230 data 206,dec,3
2240 data 208,bne,2
2250 data 209,(cmp-y),2
2260 data 213,cmp-zx,2
2270 data 214,dec-zx,2
2280 data 216,cld,1
2290 data 217,cmp-y,3
2308 data 221,cmp-x,3
2310 data 222,dec-x,3
2320 data 224,cpx#,2
2330 data 225,(sbc-x),2
2340 data 228,cpx-z,2
2350 data 229,sbc-z,2
2360 data 230,inc-z,2
2370 data 232,inx,1
2380 data 233,sbc#,2
2390 data 234,nop,1
2400 data 236,cpx,3
2410 data 237,sbc,3
2420 data 238,inc,3
2430 data 240,beq,2
2440 data 241,(sbc-y),2
2450 data 245,sbc-zx,2
2460 data 246,jnc-zx,2
2470 data 248,sed,1
2480 data 249,sbc-y,3
2490 data 253,sbc-x,3
2500 data 254,inc-x,3
2510 rem ********************************
2520 rem * header
2530 rem ********************************
2540 cr$ = "(c) copyright 1984" : nm$= "by william b. sanders"
2550 bk$ = "assembly language for kids:" : cm$= "commodore 64"
2560 is$ = "see" : f$ = "for documentation"
2570 h = 20 - len(cr$)/2 : print tab(h); cr$
2580 h = 20 - len(nm$)/2 : print tab(h); nm$
2590 print : h = 20 - len(is$)/2 : print tab(h); is$ : print
2600 h = 20 - len(bk$)/2 : print tab(h); bk$ : h = 20 - len(cm$)/2 : print tab(h); cm$
2610 h = 20 - len(nm$)/2 : print tab(h);nm$ : print
2620 h = 20 - len(f$)/2 : print tab(h); f$
2630 ld$ = "loading array" : for x = 1 to 10 : print : next : h = 20 - len(ld$)/2
2640 print tab(h); chr$(18); ld$
2650 return
