
  ;;
  ;; Assembly Language for Kids - Page 176
  ;;

  ;; Using a slightly different example, we will now look at a way to
  ;; fill up your screen with printable characters using nested loops.
  ;; Since the JSR $E716 routine outputs to screen in sequential order,
  ;; as long as we JSR $E7161000 times, we can fill the screen. Now we
  ;; know that the X and Y registers are limited to 256 (0-255) before
  ;; they burp and fall over, but by using both registers and nested 
  ;; looping, we can get up to 256 x 256 ($FFFF) passes through a loop.

  ;; ACME Assembler
  !to "176-2.o", cbm            ; name of output, type of assembly
  !cpu 6510                     ; type of cpu

  * = $c000

  jsr $e544
  ldy #$00

loop1: ldx #$21

loop2: txa
  jsr $e716
  inx
  cpx #$7f
  bne loop2

  iny
  cpy #$0a
  bne loop1

  rts
  
