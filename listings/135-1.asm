
  ;;
  ;; Assembly Language for Kids - Page 135
  ;;

  ;; The following program loads (LDA) a value into the accumulator in
  ;; the immediate addressing mode (#), stores the value at an unused
  ;; address (STA), clears the accumulator by loading it with zero, (LDA #0),
  ;;  and then loads the accumulator from the absolute mode (LDA)
  ;; from the address it first stored the value. To show you what it did,
  ;; it prints the character for the ASCII code in the accumulator with
  ;; JSR $E716.

  ;; ACME Assembler
  !to "135-1.o", cbm ; name of output, type of assembly
  !cpu 6510 ; type of cpu

  * = $c000

  jsr $e544                     ; clear screen
  lda #$43                      ; ASCII 'C'
  sta $c050                     ; store at location $c050
  lda #$00                      ; load ACC with value of 0
  lda $c050                     ; load ACC with the value at $c050
  jsr $e716                     ; print value of ACC to screen
  rts
  
