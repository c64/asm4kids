
  ;;
  ;; Assembly Language for Kids - Page 207
  ;;

  ;; The following program will let you see the joystick values on
  ;; your screen. Remembering that the neutral position is zero, we'll
  ;; have to add 48 to that value to print the ASCII character "0" to the
  ;; screen. (ASCII 48 = "0" character.) Thus, as you move your
  ;; joystick around you will see the actual EORed number in the
  ;; $DC01 register. By using LDA $D0C1 and then EORing the ac
  ;; cumulator value with $FF, we put the joystick value into the ac
  ;; cumulator. Then by using ADC #48 to add the decimal value 48 to
  ;; the accumulator and CHROUT, we print it to the screen.

  ;; ACME Assembler
  !to "207-1.o", cbm            ; name of output, type of assembly
  !cpu 6510                     ; type of cpu

  * = $c000

  jstick = $dc01
  offset = $c100
  clear  = $e544
  fire   = $c102
  chrout = $ffd2

  jsr clear
  lda #$ff                      ; Mask for EOR
  sta offset
  lda #$40                      ; EORed value of fire button + 48
  sta fire                      ; Store for easy reference

start: lda jstick               ; Read joystick
  eor offset                    ; EOR with $ff
  clc                           ; Clear carry flag
  adc #$30                      ; Add 48
  jsr chrout                    ; Print modified joystick value to screen
  cmp fire                      ; Is it the fire button?
  beq end                       ; Terminate
  jmp start                     ; Branch back

end: rts
