
  ;;
  ;; Assembly Language for Kids - Page 195
  ;;


  ;; ACME Assembler
  !to "195-1.o", cbm            ; name of output, type of assembly
  !cpu 6510                     ; type of cpu

  * = $c000

  clear   = $e544
  scnkey  = $ff9f
  getin   = $ffe4
  chrout  = $ffd2

  jsr clear
  
scan: jsr scnkey                ; Look to see if a key is pressed
  jsr getin                     ; Put key value in ACC
  beq scan                      ; Compare with 0
  jsr chrout                    ; If not 0 print to screen

  rts
