
  ;;
  ;; Assembly Language for Kids - Page 027
  ;;

  ;; ACME Assembler
  !to "027-1.o", cbm            ; name of output, type of assembly
  !cpu 6510                     ; type of cpu

       * = $c000

       ldx #$00                 ; Load X register with 0

start: txa                      ; Transfer X register values to ACC
       sta $0400,x              ; Store ACC value in $0400 indexed by X
       inx                      ; Increment X register value
       cmp #$fe                 ; Compare ACC value with 254
       bne start                ; Branch back if not equal

       rts
