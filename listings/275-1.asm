
  ;;
  ;; Assembly Language for Kids - Page 275
  ;;

	;; To make a sound, we first store values in the registers. Then we
  ;; simply "hold" a sound with a delay loop. Even a loop of 255 will
  ;; give you a very short sound; so you may need nested loops, depen
  ;; ding on what sound you want. At the end of the delay loop, you will
  ;; null the registers by storing zeros in them.
	
  ;; ACME Assembler
  !to "275-1.o", cbm          ; name of output, type of assembly
  !cpu 6510                   ; type of cpu

  * = $C000

	sigvol 	= $D418
	atdcy1	= $D405
	surel1	= $D406
	vcreg1	= $D404
	frelo1	= $D400
	frehi1	= $D401

	lda	#$0F
	sta sigvol										; set volume to 15

	lda	#$80
	sta atdcy1										; set attach - decay to 128

	lda #$C3
	sta frelo1										; store 195 in the low frequency

	lda #$10
	sta frehi1										; store 16 in the hich frequency

	lda #$11
	sta vcreg1
	ldy #$00
set:	ldx #00
play:	inx

	cpx #$FF											; double delay loop to play sound

	bne	play
	iny
	cpy	#$64
	bne	set
	lda	#$00											; null registers with 0

	sta	vcreg1
	sta	atdcy1
	sta	surel1
	sta frelo1
	sta frehi1

	rts
