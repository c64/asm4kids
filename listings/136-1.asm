
  ;;
  ;; Assembly Language for Kids - Page 136
  ;;

  ;; When you use a soft-switch address to STA a value, it is the same
  ;; as POKEing that location in BASIC. For example, to turn your
  ;; background to black, you would STA the accumulator value in
  ;; $D021. The following little program shows you how:

  ;; ACME Assembler
  !to "136-1.o", cbm ; name of output, type of assembly
  !cpu 6510 ; type of cpu

  * = $c000

  lda #$00                      ; load ACC with the value 0
  sta $d021                     ; store at location $d021 bg-color
  rts
