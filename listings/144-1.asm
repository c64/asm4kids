
  ;;
  ;; Assembly Language for Kids - Page 144
  ;;

  ;; Likewise the
  ;; PLOT subroutine at $FFF0 reads the X and Y registers to plot the
  ;; row and column position of the cursor. For example, if we wanted
  ;; to move the cursor to the middle of the screen on a 40 by 25 matrix,
  ;; we would want to specify a location of about 12/19. Let's see how
  ;; we can use the X and Y registers to output the contents of the A
  ;; register to the middle of the screen.
  
  ;; ACME Assembler
  !to "144-1.o", cbm ; name of output, type of assembly
  !cpu 6510 ; type of cpu

  * = $C000

  jsr $E544
  ldx #$0C                      ; row number 24/2
  ldy #$13                      ; column number 40/2
  clc                           ; clear the carry flag to write reg
  jsr $FFF0                     ; plot subroutine (scrambles ACC)
  lda #$58                      ; load x / spade in ACC
  jsr $E716                     ; output to screen
  rts
  
