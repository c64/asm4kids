
  ;;
  ;; Assembly Language for Kids - Page 148
  ;;

  ;; Here, we're only going to see what happens when the INX, INY, DEX or
  ;; DEY in struction occurs. Basically, when an INX or INY instruction
  ;; is issued, +1 is added to the X or Y register. With a DEX or DEY in
  ;; struction, 1 is subtracted from the X or Y register. By transferring
  ;; the values of the X and Y registers to the accumulator and sending
  ;; the results to the screen, we can graphically see what happens.

  ;; ACME Assembler
  !to "148-1.o", cbm ; name of output, type of assembly
  !cpu 6510 ; type of cpu

  * = $C000

  jsr $E544
  ldx #$5A                      ; 90 = 
  ldy #$41                      ; 65 = A
  txa
  jsr $E716                     ; output to screen
  tya
  jsr $E716
  dex                           ; decrement X register
  txa
  jsr $E716
  iny                           ; increment Y register
  tya
  jsr $E716
  rts
  
