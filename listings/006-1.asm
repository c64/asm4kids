
  ;;
  ;; Assembly Language for Kids - Page 006
  ;;

  ;; ACME Assembler
  !to "006-1.o", cbm          ; name of output, type of assembly
  !cpu 6510                   ; type of cpu

  * = $C000

  jsr $e544                   ; jump to subroutine at $E544
  rts                         ; return to basic
