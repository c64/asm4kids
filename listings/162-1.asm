
  ;;
  ;; Assembly Language for Kids - Page 162
  ;;

  ;; Let's take a look at an example that will take values from three
  ;; consecutive two-byte addresses and print them to the screen. When
  ;; you execute the program, 'ABC will appear in the upper left hand
  ;; corner of your screen.

  ;; ACME Assembler
  !to "162-1.o", cbm ; name of output, type of assembly
  !cpu 6510 ; type of cpu

  * = $c000

  jsr $e544
  ldx #$d1                      ; Low byte target address
  stx $fb                       ; Low byte pointer
  ldx #$c0                      ; High byte target address
  stx $fc                       ; High byte pointer
  ldx #$41                      ; ASCII A (65)
  stx $c0d1                     ; Store in first target address
  inx                           ; Increment value of X register (66)
  stx $c0d3                     ; Store in second target address
  inx                           ; Increment value of X register (67)
  stx $c0d5                     ; Store in third target address
  ldy #$00                      ; Set Y register to 0
  lda ($fb),y                   ; 
  jsr $e716                     ; Output to screen with CHR$
  iny                           ; Increment value of Y register
  iny                           ; Increment value of Y register
  lda ($fb),y                   ;
  jsr $e716                     ; Output to screen with CHR$
  iny                           ; Increment value of Y register
  iny                           ; Increment value of Y register
  lda ($fb),y                   ;
  jsr $e716                     ; Output to screen with CHR$

  rts
