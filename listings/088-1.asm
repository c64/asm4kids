
  ;;
  ;; Assembly Language for Kids - Page 088
  ;;

  ;; ACME Assembler
  !to "088-1.o", cbm      ; name of output, type of assembly
  !cpu 6510                     ; type of cpu

  * = $C000

   JSR $E544
   LDA #0
   STA $D021
   LDA #5
   STA $E716
   RTS
