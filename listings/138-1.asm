
  ;;
  ;; Assembly Language for Kids - Page 138
  ;;

  ;; Let's make a simple and practical program. In fact, let's make
  ;; two. One will make all your keys repeat, and the other will turn off
  ;; your key repeat. We'll store one program at 49152 and the other at
  ;; 49200. When you SYS 49152, all your keys will repeat, and when
  ;; you SYS 49200, the repeat will be turned off. These programs can
  ;; be loaded simultaneously while you program in BASIC. You might
  ;; want to turn on the repeat function if you're working with
  ;; keyboard graphics and turn it off if you're working with text. Save
  ;; the first program under the name ON and the second OFF.
  
  ;; ACME Assembler
  !to "138-1-off.o", cbm ; name of output, type of assembly
  !cpu 6510 ; type of cpu

  * = $c000

  lda #$00
  sta $028a
  rts
