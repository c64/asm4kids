


# Assembly Language For Kids: Commodore 64

 by William B. Sanders, Ph.D. San Diego State University

 microscribe - Literate Microcomputer Documentation

 8982 Stimson Court
 San Diego, California 92129
 619/484-3884 or 619/578-4588

 ISBN 0-931145-00-7

 © 1984 by William B. Sanders
 San Diego, California


 ![Assembly Language For Kids: Commodore 64](cover.jpg) 


 **LEARN ASSEMBLY LANGUAGE PROGRAMMING** If you'd rather be one of the
 kids who writes professional quality arcade games than one who just
 plays them, learn machine/assembly language programming.


 **WHAT COMPUTER?**  Everything in this book is for the Commo dore
 64. You'll get the right information for your computer; not everyone
 else's.


 **WHO'S THIS BOOK FOR?**  If you know BASIC and want to learn the fastest
 language in programming this book is for you. (If you're an adult,
 fell them you got it for your nephew in Borneo.) This is an
 elementary book for learning to use assemblers and assembly/machine
 language programming on your Commodore 64.


 **WHICH ASSEMBLER?** Three assemblers are fully covered, and most others
 for the Commodore 64 are compatible with all pro grams. Commodore's
 assembler. The Commodore 64 Macro Assembler Development System, is
 clearly explained with lots of examples. The Merlin assembler is
 clearly explained with lots of examples. If you don't have an
 assembler, there's a simple-to-learn and use listing of the Kids'
 Assembler written in BASIC for you free in the book. The Kids'
 Assembler assembles programs for you, and it will help you learn
 about assembly/machine language programming.


 **WHAT YOU GET**

 * An Assembler and instructions on using the most popular Com modore
   64 assemblers.

 * Charts covering everything from hexadecimal - decimal conver sions
   to BASIC tokens to 651O opcodes.

 * Step-by-step, clear explanations and clear examples of assembly
   language programming,

 * Practical utility programs in assembly/machine language you can
   write yourself (and understand!).

 * Amazing graphics, stupendous sprites, booming sounds, blinding
   speed, fame, fortune and a heck of a lot of fun.


 Written by, William B. Sanders, the author of the best-selling Ele
 mentary Commodore 64; you will learn assembly language more simply
 than you thought possible.


 ---

 **NOTE:** This is a collection of all the listings in this book
 including the kidsassembler which is used to follow the examples. The
 book _Assembly Language for Kids_ is available at archive.org in
 various forms. This material is provided for educational purposes
 only.

 <https://archive.org/details/Assembly_Language_for_Kids_1985_Microcomscribe>
